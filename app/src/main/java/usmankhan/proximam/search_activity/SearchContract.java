package usmankhan.proximam.search_activity;

import android.location.Location;

import java.util.ArrayList;

import usmankhan.proximam.data.model.Response;
import usmankhan.proximam.data.model.Venue;

public interface SearchContract {

    interface SearchView{

        void showProgress();

        void hideProgress();

        void close();

        void clearSuggestions();

        void showSuggestions(final ArrayList<Venue> suggestions);

        void onResponseFailure(Throwable throwable);

        void onCurrentLocationResponseFailure(Exception exception);

        void setCurrentLocation(Location location);
    }

    /**
     * GetQueriedLocationInteractor is used for getting Venue data based on query and current location
     **/
    interface GetQueriedLocationInteractor {

        interface OnFinishedListener {

            void onFinished(Response serverResponse);

            void onFailure(Throwable t);
        }

        void getQueriedLocations(String query, Location location, OnFinishedListener onFinishedListener);
    }

    /**
     * GetCurrentLocationIntractor is used for getting current location
     **/
    interface GetCurrentLocationIntractor {

        interface OnFinishedListener {

            void onFinished(Location mLocation);

            void onFailure(Exception e);

        }

        void getCurrentLocation(OnFinishedListener onFinishedListener);
    }

}
