package usmankhan.proximam.search_activity;

import android.location.Location;

public interface SearchPresenter {

    void requestCurrentLocation();

    void updateSearch(final String query, Location location);

    void submitSearch(final String query, Location location);

    void close();

}
