package usmankhan.proximam.search_activity.impl;

import android.location.Location;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import usmankhan.proximam.data.model.ServerResponse;
import usmankhan.proximam.data.network.NetworkInterface;
import usmankhan.proximam.data.network.RetrofitInstance;
import usmankhan.proximam.search_activity.SearchContract;

public class GetQueryBasedVenuesImpl implements SearchContract.GetQueriedLocationInteractor {

    @Override
    public void getQueriedLocations(String query, Location location, final OnFinishedListener onFinishedListener) {

        NetworkInterface service = RetrofitInstance.getRetrofitInstance().create(NetworkInterface.class);
        Call<ServerResponse> call = service.getVenueDataBasedOnQuery(
                "KKHDLMF5D4UPYO1NS0KMAOR3UDZGGQ5BPFUSNQJ5EUNZMECS",
                "O0HFEVPO3KTSTCJRN51MJKPQUSELP01SV502DI0D0DNCAOST",
                String.valueOf(location.getLatitude() + ","+ String.valueOf(location.getLongitude())),
                query,
                "20190111");

        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                onFinishedListener.onFinished(response.body().getResponse());
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
