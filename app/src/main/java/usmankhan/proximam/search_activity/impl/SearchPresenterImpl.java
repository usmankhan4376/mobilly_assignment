package usmankhan.proximam.search_activity.impl;

import android.location.Location;

import usmankhan.proximam.data.model.Response;
import usmankhan.proximam.search_activity.SearchContract;
import usmankhan.proximam.search_activity.SearchPresenter;

public class SearchPresenterImpl implements
        SearchPresenter, SearchContract.GetQueriedLocationInteractor.OnFinishedListener,
        SearchContract.GetCurrentLocationIntractor.OnFinishedListener{

    private SearchContract.SearchView searchView;
    private SearchContract.GetQueriedLocationInteractor getQueriedLocationInteractor;
    private SearchContract.GetCurrentLocationIntractor getCurrentLocationIntractor;

    public SearchPresenterImpl(SearchContract.SearchView searchView,
                               SearchContract.GetQueriedLocationInteractor getQueriedLocationIntractor,
                               SearchContract.GetCurrentLocationIntractor getCurrentLocationIntractor) {
        this.searchView = searchView;
        this.getQueriedLocationInteractor = getQueriedLocationIntractor;
        this.getCurrentLocationIntractor = getCurrentLocationIntractor;
    }

    @Override
    public void onFinished(Response response) {
        if(searchView != null){
            searchView.showSuggestions(response.getVenues());
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(searchView != null){
            searchView.onResponseFailure(t);
        }
    }

    @Override
    public void requestCurrentLocation() {
        getCurrentLocationIntractor.getCurrentLocation(this);
    }

    @Override
    public void updateSearch(String query, Location location) {
        if (query.isEmpty()) {
            searchView.clearSuggestions();
        } else {
            if(location != null)
                getQueriedLocationInteractor.getQueriedLocations(query, location, this);
        }
    }

    @Override
    public void submitSearch(String query, Location location) {
        if(location != null)
            getQueriedLocationInteractor.getQueriedLocations(query, location, this);
    }

    @Override
    public void close() {
        searchView.close();
    }

    @Override
    public void onFinished(Location mLocation) {
        if(searchView != null){
            searchView.setCurrentLocation(mLocation);
        }
    }

    @Override
    public void onFailure(Exception e) {
        if(searchView != null){
            searchView.onCurrentLocationResponseFailure(e);
        }
    }
}
