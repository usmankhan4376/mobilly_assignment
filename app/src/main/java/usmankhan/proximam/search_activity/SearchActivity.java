package usmankhan.proximam.search_activity;

import android.app.SearchManager;
import android.content.Context;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import usmankhan.proximam.R;
import usmankhan.proximam.adapter.VenueAdapter;
import usmankhan.proximam.adapter.RecyclerItemClickListener;
import usmankhan.proximam.data.model.Venue;
import usmankhan.proximam.di.ContextComponent;
import usmankhan.proximam.di.ContextModule;
import usmankhan.proximam.di.DaggerContextComponent;
import usmankhan.proximam.search_activity.impl.SearchGetCurrentLocationImpl;
import usmankhan.proximam.search_activity.impl.GetQueryBasedVenuesImpl;
import usmankhan.proximam.search_activity.impl.SearchPresenterImpl;

public class SearchActivity extends AppCompatActivity implements SearchContract.SearchView{

    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private SearchPresenter searchPresenter;
    private Location currentLocation = null;

    private SearchGetCurrentLocationImpl locationImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initializeToolbarAndRecyclerView();
        initProgressBar();

        ContextComponent contextComponent = DaggerContextComponent.builder().
                contextModule(new ContextModule(this)).build();
        contextComponent.injectCurrentLocationImpl(locationImpl = new SearchGetCurrentLocationImpl());

        searchPresenter = new SearchPresenterImpl(this,
                new GetQueryBasedVenuesImpl(), locationImpl);
        searchPresenter.requestCurrentLocation();
        showProgress();
    }

    /**
     * Initializing Toolbar and RecyclerView
     */
    private void initializeToolbarAndRecyclerView() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().hide();
        }

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_location_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SearchActivity.this);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initProgressBar() {
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }

    /**
     * RecyclerItem click event listener
     * */
    private RecyclerItemClickListener recyclerItemClickListener = new RecyclerItemClickListener() {
        @Override
        public void onItemClick(Venue venue) {
            Toast.makeText(SearchActivity.this,
                    "List title:  " + venue.getName(),
                    Toast.LENGTH_LONG).show();
        }
    };

    public boolean onOptionsItemSelected(MenuItem item){
        finish();
        return true;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void clearSuggestions() {
        VenueAdapter adapter = new VenueAdapter(new ArrayList<Venue>() , recyclerItemClickListener);
        recyclerView.setAdapter(adapter);
        hideProgress();
    }

    @Override
    public void showSuggestions(ArrayList<Venue> suggestions) {
        VenueAdapter adapter = new VenueAdapter(suggestions , recyclerItemClickListener);
        recyclerView.setAdapter(adapter);
        hideProgress();
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(SearchActivity.this,
                "Something went wrong...Error message: " + throwable.getMessage(),
                Toast.LENGTH_LONG).show();
        hideProgress();
    }

    @Override
    public void onCurrentLocationResponseFailure(Exception exception) {
        Toast.makeText(SearchActivity.this,
                "Something went wrong...Error message: " + exception.getMessage(),
                Toast.LENGTH_LONG).show();
        hideProgress();
    }

    @Override
    public void setCurrentLocation(Location location) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().show();
        } currentLocation = location;
        hideProgress();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        if (searchItem != null) {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            final SearchView searchView = (SearchView) searchItem.getActionView();
            if (searchView != null) {
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                searchView.setIconified(false);
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        searchView.clearFocus();
                        searchPresenter.submitSearch(s, currentLocation);
                        showProgress();
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        searchPresenter.updateSearch(s, currentLocation);
                        showProgress();
                        return true;
                    }
                });
                searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        searchPresenter.close();
                        return false;
                    }
                });
            }
        }
        return true;
    }

}
