package usmankhan.proximam.di;


import dagger.Component;
import usmankhan.proximam.impl.GetCurrentLocationImpl;
import usmankhan.proximam.main_activity.permission.PermissionActionFactory;
import usmankhan.proximam.search_activity.impl.SearchGetCurrentLocationImpl;

@Component (modules = {ContextModule.class})
public interface ContextComponent {

    void injectLocationImpl(GetCurrentLocationImpl locationImpl);
    void injectPermissionImpl(PermissionActionFactory permissionActionFactory);

    void injectCurrentLocationImpl(SearchGetCurrentLocationImpl qLocationImpl);


}
