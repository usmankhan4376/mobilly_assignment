package usmankhan.proximam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Meta implements Serializable, Parcelable {

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("requestId")
    @Expose
    private String requestId;

    public final static Creator<Meta> CREATOR = new Creator<Meta>() {

        @SuppressWarnings({
                "unchecked"
        })
        public Meta createFromParcel(Parcel in) {
            return new Meta(in);
        }

        public Meta[] newArray(int size) {
            return (new Meta[size]);
        }

    };

    protected Meta(Parcel in) {
        this.code = ((int) in.readValue((Integer.class.getClassLoader())));
        this.requestId = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Meta() {
    }

    public void setCode(int code) {
        this.code=code;
    }

    public void setRequestId(String requestId) {
        this.requestId=requestId;
    }

    public int getCode() {return this.code;}
    public String getRequestId() {return this.requestId;}


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(requestId);
    }

    public int describeContents() {
        return  0;
    }

}
