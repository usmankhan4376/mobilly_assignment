package usmankhan.proximam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by hp on 1/12/2019.
 */

public class ServerResponse implements Serializable, Parcelable {

    @SerializedName("meta")
    @Expose
    private Meta meta;

    @SerializedName("response")
    @Expose
    private Response response;

    public final static Creator<ServerResponse> CREATOR = new Creator<ServerResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ServerResponse createFromParcel(Parcel in) {
            return new ServerResponse(in);
        }

        public ServerResponse[] newArray(int size) {
            return (new ServerResponse[size]);
        }

    };

    private final static long serialVersionUID = -228523579028208802L;

    protected ServerResponse(Parcel in) {
        this.meta = ((Meta) in.readValue((Meta.class.getClassLoader())));
        this.response = ((Response) in.readValue((Response.class.getClassLoader())));
    }

    public ServerResponse() {}

    public Meta getMeta() {return this.meta;}
    public void setMeta(Meta meta) {this.meta = meta;}

    public Response getResponse() {return this.response;}
    public void setResponse(Response response) { this.response = response;}

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(meta);
        dest.writeValue(response);
    }

    public int describeContents() {
        return  0;
    }

}
