
package usmankhan.proximam.data.model;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LabeledLatLng implements Serializable, Parcelable
{

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    public final static Creator<LabeledLatLng> CREATOR = new Creator<LabeledLatLng>() {


        @SuppressWarnings({
            "unchecked"
        })
        public LabeledLatLng createFromParcel(Parcel in) {
            return new LabeledLatLng(in);
        }

        public LabeledLatLng[] newArray(int size) {
            return (new LabeledLatLng[size]);
        }

    }
    ;
    private final static long serialVersionUID = 8459080509215832799L;

    protected LabeledLatLng(Parcel in) {
        this.label = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((Double) in.readValue((Double.class.getClassLoader())));
        this.lng = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    public LabeledLatLng() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(label);
        dest.writeValue(lat);
        dest.writeValue(lng);
    }

    public int describeContents() {
        return  0;
    }

}
