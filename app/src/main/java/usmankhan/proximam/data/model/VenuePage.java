
package usmankhan.proximam.data.model;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VenuePage implements Serializable, Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    public final static Creator<VenuePage> CREATOR = new Creator<VenuePage>() {


        @SuppressWarnings({
            "unchecked"
        })
        public VenuePage createFromParcel(Parcel in) {
            return new VenuePage(in);
        }

        public VenuePage[] newArray(int size) {
            return (new VenuePage[size]);
        }

    }
    ;
    private final static long serialVersionUID = 5092238722998756435L;

    protected VenuePage(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
    }

    public VenuePage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
    }

    public int describeContents() {
        return  0;
    }

}
