package usmankhan.proximam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Response implements Serializable, Parcelable {

    @SerializedName("venues")
    @Expose
    private ArrayList<Venue> venues = null;

    public final static Creator<Response> CREATOR = new Creator<Response>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        public Response[] newArray(int size) {
            return (new Response[size]);
        }

    };

    protected Response(Parcel in) {
        in.readList(this.venues, (usmankhan.proximam.data.model.Venue.class.getClassLoader()));
    }

    public Response() {

    }

    public ArrayList<Venue> getVenues() {
        return venues;
    }

    public void setVenues(ArrayList<Venue> venues) {
        this.venues = venues;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(venues);
    }

    public int describeContents() {
        return  0;
    }

}
