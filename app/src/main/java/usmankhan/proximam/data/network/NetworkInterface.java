package usmankhan.proximam.data.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import usmankhan.proximam.data.model.ServerResponse;

public interface NetworkInterface {


    @GET("venues/search")
    Call<ServerResponse> getVenueData(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("ll") String ll,
            @Query("v") String version
    );

    @GET("venues/search")
    Call<ServerResponse> getVenueDataBasedOnQuery(
            @Query("client_id") String clientId,
            @Query("client_secret") String clientSecret,
            @Query("ll") String ll,
            @Query("query") String query,
            @Query("v") String version
    );
}
