package usmankhan.proximam.impl;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

import usmankhan.proximam.main_activity.MainActivity;
import usmankhan.proximam.main_activity.MainContract;

public class GetCurrentLocationImpl implements MainContract.GetCurrentLocationInteractor {

    private static final String TAG = "GCLIntractorImpl";

    @Inject
    Context context;

    public GetCurrentLocationImpl() {
    }

    @Override
    public void getCurrentLocation(final OnFinishedListener onFinishedListener) {
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L,
                    100.0f, new LocationListener() {

                @Override
                public void onLocationChanged(Location location) {
                    Log.d(TAG, "Location Changed");
                    onFinishedListener.onFinished(location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                    Log.d(TAG, "Status Changed");
                }

                @Override
                public void onProviderEnabled(String s) {
                    Log.d(TAG, "Provider Enabled");
                }

                @Override
                public void onProviderDisabled(String s) {
                    Log.d(TAG, "Provider Disabled");
                    onFinishedListener.onFailure(new Exception(s));
                }
            });
        } catch (SecurityException e) {
            throw new SecurityException("Access Denied");
        } catch (NullPointerException npe) {
            throw new NullPointerException("Null Pointer Exception");
        }
    }
}
