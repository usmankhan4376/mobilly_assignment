package usmankhan.proximam.adapter;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import usmankhan.proximam.R;
import usmankhan.proximam.data.model.Venue;

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.LocationViewHolder> {

    private ArrayList<Venue> dataList;
    private RecyclerItemClickListener recyclerItemClickListener;

    public VenueAdapter(ArrayList<Venue> dataList , RecyclerItemClickListener recyclerItemClickListener) {
        this.dataList = dataList;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_venue_row, parent, false);
        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.tvVenueName.setText(dataList.get(position).getName());
        holder.tvVenueAddress.setText(dataList.get(position).getLocation().getAddress());
        holder.tvVenueDistance.setText(String.valueOf(dataList.get(position).getLocation().getDistance() + " meter(s)"));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerItemClickListener.onItemClick(dataList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {

        TextView tvVenueName, tvVenueAddress, tvVenueDistance;

        LocationViewHolder(View itemView) {
            super(itemView);
            tvVenueName =  (TextView) itemView.findViewById(R.id.tvVenueName);
            tvVenueAddress =  (TextView) itemView.findViewById(R.id.tvVenueAddress);
            tvVenueDistance =  (TextView) itemView.findViewById(R.id.tvVenueDistance);

        }
    }

}
