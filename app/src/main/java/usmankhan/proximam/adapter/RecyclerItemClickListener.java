package usmankhan.proximam.adapter;

import usmankhan.proximam.data.model.Venue;

public interface RecyclerItemClickListener {

    void onItemClick(Venue venue);

}
