package usmankhan.proximam.main_activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import usmankhan.proximam.R;
import usmankhan.proximam.adapter.RecyclerItemClickListener;
import usmankhan.proximam.adapter.VenueAdapter;
import usmankhan.proximam.di.ContextComponent;
import usmankhan.proximam.di.ContextModule;
import usmankhan.proximam.di.DaggerContextComponent;
import usmankhan.proximam.main_activity.base.BaseActivity;
import usmankhan.proximam.impl.GetCurrentLocationImpl;
import usmankhan.proximam.main_activity.impl.GetVenueInteractorImpl;
import usmankhan.proximam.main_activity.impl.MainPresenterImpl;
import usmankhan.proximam.main_activity.permission.Action;
import usmankhan.proximam.main_activity.permission.PermissionAction;
import usmankhan.proximam.main_activity.permission.PermissionActionFactory;
import usmankhan.proximam.data.model.Venue;
import usmankhan.proximam.search_activity.SearchActivity;

public class MainActivity extends BaseActivity implements
        MainContract.MainView, PermissionPresenter.PermissionCallbacks {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private MainContract.presenter presenter;
    private PermissionPresenter permissionPresenter;

    private GetCurrentLocationImpl locationImpl;
    private PermissionActionFactory permissionActionFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeToolbarAndRecyclerView();
        initProgressBar();

        ContextComponent contextComponent = DaggerContextComponent.builder().
                contextModule(new ContextModule(this)).build();

        contextComponent.injectLocationImpl(locationImpl = new GetCurrentLocationImpl());
        contextComponent.injectPermissionImpl(permissionActionFactory = new PermissionActionFactory());

        presenter = new MainPresenterImpl(this, new GetVenueInteractorImpl(), locationImpl);

        PermissionAction permissionAction = permissionActionFactory.getPermissionAction();

        permissionPresenter = new PermissionPresenter(permissionAction, this);

        requestLocationFinePermission();
    }

    /**
     * Initializing Toolbar and RecyclerView
     */
    private void initializeToolbarAndRecyclerView() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null)
            getSupportActionBar().hide();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_location_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
    }

    /**
     * Initializing progressbar programmatically
     * */
    private void initProgressBar() {
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }

    /**
     * RecyclerItem click event listener
     * */
    private RecyclerItemClickListener recyclerItemClickListener = new RecyclerItemClickListener() {
        @Override
        public void onItemClick(Venue venue) {
            Toast.makeText(MainActivity.this,
                    "List title:  " + venue.getName(),
                    Toast.LENGTH_LONG).show();
        }
    };

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setDataToRecyclerView(ArrayList<Venue> venues) {
        VenueAdapter adapter = new VenueAdapter(venues , recyclerItemClickListener);
        recyclerView.setAdapter(adapter);
        hideProgress();
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(MainActivity.this,
                "Something went wrong...Error message: " + throwable.getMessage(),
                Toast.LENGTH_LONG).show();
        hideProgress();
    }

    @Override
    public void requestLocationFinePermission() {
        permissionPresenter.requestFineLocationPermission();
    }

    @Override
    public void onCurrentLocationResponseFailure(Exception exception) {
        hideProgress();
        Toast.makeText(MainActivity.this,
                "Something went wrong...Error message: " + exception.getMessage(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void setCurrentLocation(Location location) {
        presenter.requestDataFromServer(location);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.search){
            Intent i = new Intent(MainActivity.this, SearchActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }


    //----- Permission management ----//

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Action.ACCESS_FINE_LOCATION:
                permissionPresenter.checkGrantedPermission(grantResults, requestCode);
        }
    }

    @Override
    public void permissionAccepted(int action) {
        switch (action) {
            case Action.ACCESS_FINE_LOCATION:
                showProgress();
                if(getSupportActionBar() !=null)
                    getSupportActionBar().show();
                presenter.requestCurrentLocation();
                break;
        }
    }

    @Override
    public void permissionDenied(int action) {
        if (dismissPermissionRationale() == 0) {
            showSnackBarPermissionMessage(action);
        }
    }

    @Override
    public void showRationale(int action) {
        switch (action) {
            case Action.ACCESS_FINE_LOCATION:
                createAndShowPermissionRationale(action, R.string.title,
                        R.string
                        .content);
                break;
        }
    }

    public void onAcceptRationaleClick(View view) {
        int action = dismissPermissionRationale();
        switch (action) {
            case Action.ACCESS_FINE_LOCATION:
                permissionPresenter.requestPermission(Action.FINE_LOCATION);
                break;
        }
    }

    @Override
    protected void showSnackBarPermissionMessage(int action) {
        switch (action) {
            case Action.ACCESS_FINE_LOCATION:
                super.showSnackBarPermissionMessage(R.string.content);
                break;
        }
    }
}
