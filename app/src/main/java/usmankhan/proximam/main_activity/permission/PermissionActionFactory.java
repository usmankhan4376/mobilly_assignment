package usmankhan.proximam.main_activity.permission;

import android.content.Context;

import javax.inject.Inject;

import usmankhan.proximam.main_activity.impl.SupportPermissionActionImpl;

public class PermissionActionFactory {

    @Inject
    Context context;

    public PermissionActionFactory() {}

    public PermissionAction getPermissionAction() {
        return new SupportPermissionActionImpl(context);
    }

}
