package usmankhan.proximam.main_activity.permission;

import android.Manifest;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Action {

    @IntDef({ ACCESS_FINE_LOCATION})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ActionCode {
    }

    public static final int ACCESS_FINE_LOCATION = 0;

    public static final Action FINE_LOCATION = new Action(ACCESS_FINE_LOCATION, Manifest.permission
            .ACCESS_FINE_LOCATION);

    private int code;
    private String permission;

    private Action(@ActionCode int value, String name) {
        this.code = value;
        this.permission = name;
    }

    public int getCode() {
        return code;
    }

    public String getPermission() {
        return permission;
    }

}
