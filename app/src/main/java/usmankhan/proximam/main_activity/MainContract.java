package usmankhan.proximam.main_activity;

import android.location.Location;

import java.util.ArrayList;

import usmankhan.proximam.data.model.Response;
import usmankhan.proximam.data.model.Venue;

public interface MainContract {

    interface presenter{

        void onDestroy();

        void requestDataFromServer(Location location);

        void requestCurrentLocation();

    }

    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToRecyclerView(ArrayList<Venue> response);

        void onResponseFailure(Throwable throwable);

        void requestLocationFinePermission();

        void onCurrentLocationResponseFailure(Exception exception);

        void setCurrentLocation(Location location);

    }

    /**
     * GetVenueDataInteractor is used for getting Venue data
     **/
    interface GetVenueDataInteractor {

        interface OnFinishedListener {

            void onFinished(Response serverResponse);

            void onFailure(Throwable t);

        }

        void getLocations(Location location, OnFinishedListener onFinishedListener);
    }

    /**
     * GetCurrentLocationInteractor is used for getting current location
     **/
    interface GetCurrentLocationInteractor {

        interface OnFinishedListener {

            void onFinished(Location mLocation);

            void onFailure(Exception e);

        }

        void getCurrentLocation(OnFinishedListener onFinishedListener);
    }

}
