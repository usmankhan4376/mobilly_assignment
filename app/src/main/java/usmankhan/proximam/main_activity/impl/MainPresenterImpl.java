package usmankhan.proximam.main_activity.impl;

import android.location.Location;

import usmankhan.proximam.main_activity.MainContract;
import usmankhan.proximam.data.model.Response;

public class MainPresenterImpl implements
        MainContract.presenter, MainContract.GetVenueDataInteractor.OnFinishedListener,
        MainContract.GetCurrentLocationInteractor.OnFinishedListener{

    private MainContract.MainView mainView;
    private MainContract.GetVenueDataInteractor getVenueDataInteractor;
    private MainContract.GetCurrentLocationInteractor getCurrentLocationInteractor;

    public MainPresenterImpl(MainContract.MainView mainView,
                             MainContract.GetVenueDataInteractor getLocationIntractor,
                             MainContract.GetCurrentLocationInteractor getCurrentLocationInteractor) {
        this.mainView = mainView;
        this.getVenueDataInteractor = getLocationIntractor;
        this.getCurrentLocationInteractor = getCurrentLocationInteractor;
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void requestDataFromServer(Location location) {
        getVenueDataInteractor.getLocations(location, this);
    }

    @Override
    public void requestCurrentLocation() {
        getCurrentLocationInteractor.getCurrentLocation(this);
    }

    @Override
    public void onFinished(Response response) {
        if(mainView != null){
            mainView.setDataToRecyclerView(response.getVenues());
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView != null){
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFinished(Location mLocation) {
        if(mainView != null){
            mainView.setCurrentLocation(mLocation);
        }
    }

    @Override
    public void onFailure(Exception e) {
        if(mainView != null){
            mainView.onCurrentLocationResponseFailure(e);
        }
    }
}
