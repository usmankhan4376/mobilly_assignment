package usmankhan.proximam.main_activity.impl;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import javax.inject.Inject;

import usmankhan.proximam.main_activity.permission.PermissionAction;

public class SupportPermissionActionImpl implements PermissionAction {

    Context context;

    public SupportPermissionActionImpl(Context context) {this.context = context;}

    @Override
    public boolean hasSelfPermission(String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void requestPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions((Activity) context, new String[] { permission }, requestCode);
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission);
    }

}
