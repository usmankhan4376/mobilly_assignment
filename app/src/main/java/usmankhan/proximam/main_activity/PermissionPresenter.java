package usmankhan.proximam.main_activity;

import android.content.pm.PackageManager;

import usmankhan.proximam.main_activity.permission.Action;
import usmankhan.proximam.main_activity.permission.PermissionAction;


public class PermissionPresenter {

    private PermissionAction permissionAction;
    private PermissionCallbacks permissionCallbacks;

    public PermissionPresenter(PermissionAction permissionAction, PermissionCallbacks permissionCallbacks) {
        this.permissionAction = permissionAction;
        this.permissionCallbacks = permissionCallbacks;
    }

    public void requestFineLocationPermission() {
        checkAndRequestPermission(Action.FINE_LOCATION);
    }

    private void checkAndRequestPermission(Action action) {
        if (permissionAction.hasSelfPermission(action.getPermission())) {
            permissionCallbacks.permissionAccepted(action.getCode());
        }else {
            if (permissionAction.shouldShowRequestPermissionRationale(action.getPermission())) {
                permissionCallbacks.showRationale(action.getCode());
            } else {
                permissionAction.requestPermission(action.getPermission(), action.getCode());
            }
        }
    }

    public void requestPermission(Action action) {
        permissionAction.requestPermission(action.getPermission(), action.getCode());
    }

    public void checkGrantedPermission(int[] grantResults, int requestCode) {
        if (verifyGrantedPermission(grantResults)) {
            permissionCallbacks.permissionAccepted(requestCode);
        } else {
            permissionCallbacks.permissionDenied(requestCode);
        }
    }

    private boolean verifyGrantedPermission(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public interface PermissionCallbacks {

        void permissionAccepted(@Action.ActionCode int actionCode);

        void permissionDenied(@Action.ActionCode int actionCode);

        void showRationale(@Action.ActionCode int actionCode);

    }

}
